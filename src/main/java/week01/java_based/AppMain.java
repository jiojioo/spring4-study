package week01.java_based;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import week01.models.Week1Person;

public class AppMain {

	public static void main(String[] args) {
		ApplicationContext context = new AnnotationConfigApplicationContext(Configure.class);
		Week1Person itemFromContext = context.getBean(Week1Person.class);
		System.out.println(itemFromContext);

	}
}
